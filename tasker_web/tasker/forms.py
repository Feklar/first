from django import forms
import sys
sys.path.append('..')
from library import database

class TaskForm(forms.Form):
    name=forms.CharField()
    execute_time=forms.DateTimeField(required=False,
                                     input_formats=['%Y-%m-%dT%H:%M'],
                                     widget=forms.DateTimeInput(
                                        attrs={'type':"datetime-local",
                                               id:'execute_time'}))
    duration=forms.DurationField(required=False)
    tags=forms.CharField(required=False)
    priority=forms.IntegerField(required=False)
    shared_users=forms.CharField(required=False)
    completed=forms.BooleanField(required=False)

class RepeatForm(forms.Form):
    type=forms.ChoiceField(choices=((0, 'Interval'), (1, 'Weekdays'), (2, 'Dates')))
    when=forms.CharField(required=False)
