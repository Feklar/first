"""This module contains classes for work with tasks"""

import calendar
import datetime
import sys
import logging
import abc


#WEEKDAYS_NAMES = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday',
#'saturday', 'sunday']
#WEEKDAYS_NAMES = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
WEEKDAYS_NAMES = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su']
class OvertaskRepeatError(Exception):
    pass


class Repeat:

    """Rules for repeat
    
    Attributes:
        type (int): value """

    (INTERVAL, WEEKDAY, DATE) = range(3)

    def __init__(self, interval=None, weekdays=None, dates=None):
        if [interval, weekdays, dates].count(None) != 2:
            raise ValueError("Exactly one of parameters must be not None")
        if interval is not None:
            self.type = Repeat.INTERVAL
            self.when = interval
        if weekdays is not None:
            self.type = Repeat.WEEKDAY
            self.when = weekdays
        if dates is not None:
            self.type = Repeat.DATE
            self.when = dates

    def time_to(self, _from):
        if self.type == Repeat.INTERVAL:
            return self.when
        if self.type == Repeat.WEEKDAY:
            weekday = _from.weekday()
            for day in self.when:
                if day > weekday:
                    return datetime.timedelta(days=day - weekday)
            return datetime.timedelta(days=7 + self.when[0] - weekday)
        if self.type == Repeat.DATE:
            date = _from.day
            for day in self.when:
                if day > date:
                    return datetime.timedelta(days=day - date)
            monthdays = calendar.mdays[_from.month]
            is_leap_february = _from.month == calendar.February and calendar.isleap(_from.year)
            if is_leap_february:
                monthdays += 1
            return datetime.timedelta(days=monthdays + self.when[0] - date)

    def __str__(self):
        if self.type == Repeat.INTERVAL:
            return 'each ' + str(self.when)
        if self.type == Repeat.WEEKDAY:
            return 'each ' + ', '.join([WEEKDAYS_NAMES[weekday] for weekday in
            self.when])
        if self.type == Repeat.DATE:
            return 'each ' + ', '.join([str(day) for day in self.when]) + 'day of the month'

class Task:

    """Class represents a task
    
    Attributes:
        id (int): unique task id
        name (str): name of a task
        subtasks (list): list of subtasks
        execute_time (datetime.datetime): time of execution's begin
        duration (datetime.timedelta): durarion of execution
        repeat (Repeat): rules of task repeat
        overtask (Task): parent task
        completed (Bool): True if task is completed
        creator (Object): creator of a task, type not defined in this module
        tags (list): list of a tags
        priority (int): task's priority, greater value mean important
        shared_users (Object): list of users having acess
        """

    def __init__(self):
        self.id = 0
        self.name = ''
        self.subtasks = []
        self.execute_time = None
        self.duration = datetime.timedelta(0)
        self.repeat = None
        self.overtask = None
        self.completed = False
        self.creator = None
        self.tags = []
        self.priority = None
        self.shared_users = []

    def add_subtask(self, task):
        self.subtasks.append(task)
        task.overtask = self

    def set_repeat(self, repeat):
        if self.overtask is not None:
            raise OvertaskRepeatError()
        self.repeat = repeat

    def complete(self):
        self.completed = True

    def get_repeated(self, _from=None):
        """Returns new task which is a copy of self but execute_time shifted corresponding 
        with self.repeat"""
        if self.repeat is not None:
            new_task = Task()
            new_task.name = self.name
            new_task.creator = self.creator
            new_task.duration = self.duration
            new_task.overtask = self.overtask
            new_task.completed = False
            new_task.set_repeat(self.repeat)
            if _from is None:
                _from = self.execute_time
            else:
                _from = max(_from, self.execute_time)
            new_datetime = (_from + self.repeat.time_to(_from))
            new_task.execute_time = new_datetime
            new_task.duration = self.duration
            new_task.repeat = self.repeat
            new_task.priority = self.priority
            new_task.tags=self.tags.copy()
            return new_task
        else:
            return None

    def get_description(self):
        """Returns detail description if a task"""
        description = 'Task [%s] %s\n' % (self.id, self.name)
        if self.creator is not None:
            description+='Created by %s\n' % self.creator
        if len(self.shared_users) > 0:
            description+='Aviable for: %s\n' % ', '.join(str(user) for user in self.shared_users)
        if self.overtask is not None:
            description+='Is subtask of %s\n' % self.overtask
        if len(self.subtasks) > 0:
            description+='Have %d subtasks\n' % len(self.subtasks)
        if self.execute_time is not None:
            description+='Start time of execution: %s\n' % self.execute_time
        if self.duration is not None:
            description+='During: %s\n' % self.duration
        description+='Status: %s\n' % ('Completed' if self.completed else 'Not completed')
        if self.repeat is not None:
            description+='Repeat: %s\n' % self.repeat
        if len(self.tags) > 0:
            description+='Tags: %s\n' % ', '.join(tag for tag in self.tags)
        description+='Priority: %s\n' % self.priority
        return description

    def __str__(self):
        """Returns short description if a task"""
        string = '[%s]' % self.id
        if self.name is not None:
            string += " '" + self.name + "'"
        if self.creator is not None:
            string += ' by ' + str(self.creator)
        if self.overtask is not None:
            string += ' Overtask: ' + str(self.overtask.id)
        if self.execute_time is not None:
            string += ' ' + str(self.execute_time.date())
        if self.completed:
            string += ' Completed'
        if self.tags is not None:
            string += ' Tags: ' + ', '.join(tag for tag in self.tags) + ';'
        if self.priority is not None:
            string += ' Priority: ' + str(self.priority)
        return string
