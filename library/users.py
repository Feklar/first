"""This module contains classes for work with users"""

class InvalidPasswordError(Exception):
    pass

class UserNotAutorizedError(Exception):
    login = None
    def __init__(self, login):
        self.login = login

class User:
    def __init__(self):
        self.id = None
        self.login = None
        self.password = None
        self.autorized = False

    def autorize(self, password):
        if password == self.password:
            self.autorized = True
        else:
            raise InvalidPasswordError()

    def deautorize(self):
        self.autorized = False

    def get_description(self):
        description = 'User [%s] %s\n' % (self.id, self.login)
        if self.autorized:
            description+='Autorized\n'
        return description

    def autentificate(self):
        if not self.autorized:
            raise UserNotAutorizedError(self.login)

    def __str__(self):
        return self.login

    def __int__(self):
        return self.id
