import logging
from logging import (
    NOTSET,
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    CRITICAL)

DETAIL = INFO - 5
logging.addLevelName(DETAIL, "DETAIL")

logger = logging.getLogger()


def logged(func):
    def logged_func(*args, **kwargs):
        logger.log(INFO, "Calling function '%s'" % func.__name__)
        logger.log(DETAIL, "Arguments: %s %s" % (args, kwargs))
        try:
            result = func(*args, **kwargs)    
        except BaseException as e:
            logger.log(DETAIL, "Function '%s' raise %s" % (func.__name__, repr(e)))
            raise
        logger.log(DETAIL, "Function '%s' return %s" % (func.__name__, repr(result)))
        return result
    return logged_func
