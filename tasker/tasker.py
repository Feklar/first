#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import datetime
#import argcomplete
import sys
import os 
from tasker import settings
import library.lib as lib
import library.users as users
import library.database as db
import logger.logger as logger

class Error(Exception):
    '''Error which will be shown to user'''
    message = None
    def __init__(self, message):
        self.message = message

def try_to_int(arg):
    '''Return int(arg) if it is possible, else return arg'''
    if arg is None:
        return None
    try:
        return int(arg)
    except ValueError:
        return arg

@logger.logged
def get_task(id):
    tasks = db.load_tasks(id=id)
    if len(tasks) == 0:
        raise Error('Task not exist')
    elif len(tasks) > 1:
        raise Error('Multiple tasks exist. Use ID')
    return tasks[0]

@logger.logged
def add_task(args):
    task = lib.Task()
    task.name = args.name
    task.overtask = args.overtask
    if args.creator is not None:
        args.creator.autentificate()
        task.creator = args.creator
    if args.when == 'now':
        task.execute_time = datetime.datetime.now()
    else:
        task.execute_time = datetime.datetime.strptime(args.when, settings.DATETIME_FORMAT)
    if args.duration is not None:
        d = datetime.datetime.strptime(args.duration, "%H:%M:%S")
        task.duration = datetime.timedelta(hours=d.hour, minutes=d.minute, seconds=d.second)
    task.tags = args.tags
    task.priority = args.priority
    db.save_task(task)
    print("Task created: " + str(task))

@logger.logged
def edit_task(args):
    if args.overtask is not None:
        args.task.overtask = args.overtask
    if args.creator is not None:
        args.task.creator = args.user
    if args.when is not None:
        if args.when == 'now':
            args.task.execute_time = datetime.datetime.now()
        else:
            args.task.execute_time = datetime.datetime(args.when)
    if args.set_tags is not None:
        args.task.tags = args.set_tags.copy()
    if args.add_tags is not None:
        args.task.tags.extend(args.add_tags)
    if args.priority is not None:
        args.task.priority = args.priority
    db.save_task(args.task)

@logger.logged
def show_tasks(args):
    tasks = db.load_tasks(name=args.name, creator=args.creator, completed=args.completed, 
                          overtask=args.overtask, id=args.id, tags_any=args.tags_any, 
                          tags_all=args.tags_all)
    for task in tasks:
        if args.verbose:
            print(task.get_description())
        else:
            print(task)
    
@logger.logged
def show_users(args):
    users = db.load_users(id=args.id, login=args.login, autorized=args.autorized)
    for user in users:
        if args.verbose:
            print(user.get_description())
        else:
            print(user)

@logger.logged
def add_repeat(args):
    interval = None
    if args.interval is not None:
        interval = datetime.datetime.strptime(args.interval, "%H:%M:%S") - \
                   datetime.datetime.strptime("0:0:0", "%H:%M:%S")
    repeat = lib.Repeat(weekdays=args.weekdays, dates=args.dates, interval=interval)
    task = args.task
    task.set_repeat(repeat)
    db.save_task(task)

@logger.logged
def complete(args):
    if not args.task.creator.autorized:
        for shared_user in args.task.shared_users:
            if shared_user.autorized: break
        else:
            print("Autorized users dont have access")
            return
    subtasks = db.load_tasks(overtask=args.task, completed=False)
    if len(subtasks) > 0:
        if not args.subtasks:
            print("There are uncompleted subtasks. Use -s for complete them or complete manually.")
            return
        else:
            for subtask in subtasks:
                complete(argparse.Namespace(id=subtask.id, overtask=task, subtasks=args.subtasks, name=None))
    args.task.complete()
    db.update_task(args.task)
    new_task = args.task.get_repeated(datetime.datetime.now())
    if new_task is not None:
        if 'overtask' in args:
            new_task.overtask = args.overtask.id
        db.save_task(new_task)

@logger.logged
def create_user(args):
    user = users.User()
    user.login = args.login
    user.password = args.password
    db.save_user(user)

@logger.logged
def autorize(args):
    if args.login is None:
        print('Enter user login:')
        args.login = input()
    _users = db.load_users(login=args.login)
    # _users start with _ because of conflict with module users
    if len(_users) < 1:
        print('User %s is not exist' % (args.login,))
        return
    user = _users[0]
    if args.password is None:
        print('Enter user password:')
        args.password = input()
    try:
        user.autorize(args.password)
    except users.InvalidPasswordError:
        print('Invalid password')
        print('Correct password is %s' % (user.password,)) #Not forget remove on release!
    db.update_user(user)
    print('User %s autorized' % (user.login,))

@logger.logged
def deautorize(args):
    if args.login is None:
        print('Enter user login:')
        args.login = input()
    _users = db.load_users(login=args.login)
    # _users start with _ because of conflict with module users
    if len(_users) < 1:
        print('User %s is not exist' % (args.login,))
        return
    user = _users[0]
    user.deautorize()
    db.update_user(user)
    print('User %s deautorized' % (user.login,))

@logger.logged
def share(args):
    if not task.creator.autorized:
        print("Task creator is not autorized")
        return
    for u in args.users:
        user = db.load_users(login=u)[0]
        if user.id in (u.id for u in task.shared_users):
            continue
        task.shared_users.append(db.load_users(login=u)[0])
    db.update_task(task)
    db.update_shares(task)

@logger.logged
def archive(args):
    tasks = db.load_tasks(name=args.name, id=args.id)
    try:
        task = db.single(tasks)
    except db.EmptyListError:
        print("Task already completed or not exist")
        return
    except db.TooBigListError:
        print("Multiple tasks exist. Use ID")
        return
    if not task.creator.autorized:
        print("Task creator is not autorized")
        return
    db.move_to_archive(task)

def init_task_subparser(subparsers):
    task_parser = subparsers.add_parser('task')
    task_parser.add_argument('--task', '-t', type=str, action='store',
                                        help='Task ID')

    def init_add_subparser(subparsers):
        add_subparser = subparsers.add_parser('add', help='Create a task')
        add_subparser.add_argument('--name', '-n', type=str, action='store', required=True,
                                        help='Name of a task')
        add_subparser.add_argument('--creator', '-c', type=str, action='store',
                                        help='Creator of a task')
        add_subparser.add_argument('--when', '-w', type=str, action='store', default='now', help='Execution time')
        add_subparser.add_argument('--duration', '-d', type=str, action='store',
                                        help='Duration of execution')
        add_subparser.add_argument('--overtask', '-o', type=str, action='store',
                                        metavar='PARENT_TASK_ID', help='Parent task ID or name')
        add_subparser.add_argument('--tags', '-g', type=str, nargs='+',
                                        help='Task tags', default=[])
        add_subparser.add_argument('--priority', '-p', type=int, action='store',
                                        help='Task priority', default=0)
        add_subparser.set_defaults(f=add_task)

    def init_edit_subparser(subparsers):
        edit_subparser = subparsers.add_parser('edit', help='Edit task')
        
        edit_subparser.add_argument('--creator', '-c', type=str, action='store',
                                        help='Creator of a task')
        edit_subparser.add_argument('--when', '-w', type=str, action='store', help='Execution time')
        edit_subparser.add_argument('--duration', '-d', type=str, action='store',
                                        help='Duration of execution')
        edit_subparser.add_argument('--overtask', '-o', type=str, action='store',
                                        metavar='PARENT_TASK_ID', help='Parent task ID or name')
        edit_subparser.add_argument('--set-tags', '-G', type=str, nargs='+',
                                        help='Task tags', dest='set_tags', default=None)
        edit_subparser.add_argument('--add-tags', '-g', type=str, nargs='+',
                                        help='Task tags', dest='add_tags', default=None)
        edit_subparser.add_argument('--priority', '-p', type=int, action='store',
                                        help='Task priority', default=None)
        edit_subparser.set_defaults(f=edit_task)

    def init_complete_subparser(subparsers):
        complete_subparser = subparsers.add_parser('complete', help='Complete a task')
        complete_subparser.add_argument('--subtasks', '-s', action='store_true', default=False,
                                        help='Complete subtasks also')
        complete_subparser.set_defaults(f=complete)

    def init_repeat_subparser(subparsers):
        repeat_subparser = subparsers.add_parser('repeat', help='Set repeat')
        repeat_subparser.add_argument('--weekdays', '-w', nargs='*', type=int, action='store',
                                            help='List of days of week for repeat')
        repeat_subparser.add_argument('--dates', '-d', nargs='*', type=int, action='store',
                                            help='List of dates for repeat')
        repeat_subparser.add_argument('--interval', '-i', type=str, action='store',
                                            help='Time interval for repeat')
        repeat_subparser.set_defaults(f=add_repeat)

    def init_share_subparser(subparsers):
        share_subparser = subparsers.add_parser('share')
        share_subparser.add_argument('--users', '-u', type=str, nargs='+',
                                        action='store', help='Users logins', default=[])
        share_subparser.set_defaults(f=share)

    def init_archive_subparser(subparsers):
        complete_subparser = subparsers.add_parser('archive', help='Complete a task')
        complete_subparser.set_defaults(f=archive)

    def init_show_subparser(subparsers):
        show_subparser = subparsers.add_parser('show', help='Show all tasks with specified parameters')
        show_subparser.add_argument('--name', '-n', type=str, action='store',
                                        help='Name of a task')
        show_subparser.add_argument('--id', '-i', type=str, action='store',
                                        help='Id of a task')
        show_subparser.add_argument('--creator', '-r', type=str, action='store', help='Creator of a task')
        #show_subparser.add_argument('--without-creator', '-R', nargs=0,
        #action='store_const',
        #                           const='', dest='creator', help='Creator of
        #                           a
        #                           task')
        show_subparser.add_argument('--not-completed', '-c', action='store_false',
                                    dest='completed', default=None, help='Show not completed tasks only')
        show_subparser.add_argument('--completed', '-C', action='store_true', default=None,
                                    help='Show completed tasks only')
        show_subparser.add_argument('--overtask', '-o', type=str, action='store',
                                    metavar='PARENT_TASK_ID', help='Parent task ID or name')
        show_subparser.add_argument('--tags-any', '-g', type=str, nargs='+', dest='tags_any',
                                        help='Task tags', default=None)
        show_subparser.add_argument('--tags-all', '-G', type=str, nargs='+', dest='tags_all',
                                        help='Task tags', default=None)
        show_subparser.add_argument('--verbose', '-v', action='store_true', default=False,
                                    help='Be verbose')
        show_subparser.set_defaults(f=show_tasks)

    task_subparsers = task_parser.add_subparsers(title='Work with task')
    init_add_subparser(task_subparsers)
    init_edit_subparser(task_subparsers)
    init_complete_subparser(task_subparsers)
    init_repeat_subparser(task_subparsers)
    init_share_subparser(task_subparsers)
    init_archive_subparser(task_subparsers)
    init_show_subparser(task_subparsers)

def init_user_subparser(subparsers):
    user_parser = subparsers.add_parser('user')
    def init_create_user_subparser(subparsers):
        add_subparser = subparsers.add_parser('add', help='Create new user')
        add_subparser.add_argument('--login', '-l', type=str, action='store',
                                        required=True, help='User login')
        add_subparser.add_argument('--password', '-p', type=str, action='store',
                                        required=True, help='User password')
        add_subparser.set_defaults(f=create_user)

    def init_autorize_subparser(subparsers):
        autorize_subparser = subparsers.add_parser('autorize')
        autorize_subparser.add_argument('--login', '-l', type=str, action='store', help='User login')
        autorize_subparser.add_argument('--password', '-p', type=str, action='store', help='User password')
        autorize_subparser.set_defaults(f=autorize)

    def init_deautorize_subparser(subparsers):
        autorize_subparser = subparsers.add_parser('deautorize')
        autorize_subparser.add_argument('--login', '-l', type=str, action='store', help='User login')
        autorize_subparser.set_defaults(f=deautorize)

    def init_show_subparser(subparsers):
        show_subparser = subparsers.add_parser('show', help='Show all users with specified parameters')
        show_subparser.add_argument('--id', '-i', type=int, action='store',
                                        help='Id of a user')
        show_subparser.add_argument('--login', '-l', type=str, action='store',
                                        help='Login')
        show_subparser.add_argument('--not-autorized', '-a', action='store_false',
                                    dest='autorized', default=None, help='Show not autorized users only')
        show_subparser.add_argument('--autorized', '-A', action='store_true', default=None,
                                    help='Show autorized users only')
        show_subparser.add_argument('--verbose', '-v', action='store_true', default=False,
                                    help='Be verbose')
        show_subparser.set_defaults(f=show_users)

    user_subparsers = user_parser.add_subparsers(title='Work with user')
    init_create_user_subparser(user_subparsers)
    init_autorize_subparser(user_subparsers)
    init_deautorize_subparser(user_subparsers)
    init_show_subparser(user_subparsers)

def init_start_subparser(subparsers):
    start_subparser = subparsers.add_parser('start', help='Start working in interactive mode')
    start_subparser.set_defaults(start=True)

def init_parser():
    parser = argparse.ArgumentParser(usage='Script options "%(prog)s"\n'+
                                        'For show tasks use "task show"\n'+
                                        'For create task use "task create"\n'+
                                        "Use -h for help",
                                     prog='Tasker',
                                     description='Tasks manager',
                                     epilog='2018')
    parser.set_defaults(name=None, id=None, f=lambda _: parser.print_help())
    subparsers = parser.add_subparsers(title='Commands')

    init_task_subparser(subparsers)
    init_user_subparser(subparsers)
    init_start_subparser(subparsers)

    #argcomplete.autocomplete(parser)
    return parser

def process_args(args):
    if 'overtask' in args and args.overtask is not None:
        args.overtask = get_task(args.overtask)
    if 'task' in args.__dict__ and args.task is not None:
        args.task = get_task(args.task)
        args.id = args.task.id
        args.name = args.task.name
    if 'creator' in args.__dict__ and args.creator is not None:
        args.creator = db.load_user(login=args.creator)

def main():
    logger.logging.basicConfig(filename=settings.LOG_FILE, level=settings.LOG_LEVEL)
    db.init_database(settings.DATABASE_FILE)
    parser = init_parser()
    #args = parser.parse_args(['start'])
    args = parser.parse_args()
    try:
        process_args(args)
        if 'start' in args.__dict__ and args.start is True:
            while True:
                print('>>>')
                inp = input()
                inp = inp.split()
                if inp[0] == 'exit':
                    exit()
                args = parser.parse_args(inp)
                process_args(args)
                args.f(args)
        args.f(args)
    except Error as e:
        print(e.message)
        return
    except users.UserNotAutorizedError as e:
        print('User %s not autorized' % e.login)
        return

if __name__ == "__main__":
    main()