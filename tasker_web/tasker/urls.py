from django.conf.urls import url
from tasker import views

urlpatterns = [
    url(r'^$', views.task_list, name='task_list'),
    url(r'^user/(?P<id>\d+)$', views.user_show, name='user_show'),
    url(r'^task/(?P<id>\d+)$', views.task_show, name='task_show'),
    url(r'^add_task$', views.add_task, name='add_task'),
    url(r'^add_subtask/(?P<id>\d+)$', views.add_subtask, name='add_subtask'),
    url(r'^repeat/(?P<id>\d+)$', views.repeat, name='repeat'),
    ]