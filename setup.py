# -*- coding: utf-8 -*-
import setuptools

setuptools.setup(name='tasker',
                 version='0.1',
                 packages=setuptools.find_packages(),
                 install_requires=["peewee"],
                 entry_points={
                     'console_scripts':
                        ['tasker = tasker.tasker:main']
                 })
