"""
    This package contains classes for tasks, users and 
    models for save and load them

    Modules:
        lib.py - contains task classes
        users.py - module contains user class
        database.py - models and functions for save and load

    Examples:
        Create task:
            task=lib.Task()
            task.name='Task 1'

        Add repeat for task:
            repeat=lib.Repeat(weekdays=[1, 2, 5])
            task.set_repeat(repeat)

        Save task:
            database.save_task(task)

        Load tasks:
            task1 = database.load_tasks(name='Task 1')
            task2 = database.load_tasks(id=1)
"""