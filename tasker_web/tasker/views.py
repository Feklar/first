import django
from django.shortcuts import render
from . import forms
import sys
sys.path.append('..')
from library import lib
from library import database
import tasker_web.settings as settings

def task_list(request):
    database.init_database(settings.DATABASE_FILE)
    tasks = database.load_tasks()
    tasks=[t for t in tasks if t.overtask is None]
    return render(request, 'tasker/task_list.html', {'tasks':tasks})

def task_show(request, id):
    id=int(id)
    database.init_database(settings.DATABASE_FILE)        
    if request.method=='POST':
        form=forms.TaskForm(request.POST)
        if form.is_valid():
            task=database.load_tasks(id=id)[0]
            task.name=form.cleaned_data['name']
            task.execute_time=form.cleaned_data['execute_time']
            task.duration=form.cleaned_data['duration']
            task.tags=form.cleaned_data['tags'].split()
            task.priority=form.cleaned_data['priority']
            task.shared_users=[(database.load_user(login=l))
                              for l in form.cleaned_data['shared_users'].split()]
            if not task.completed and form.cleaned_data['completed']:
                task.complete()
            if not form.cleaned_data['completed']:
                task.completed=False
            database.save_task(task)
            return django.shortcuts.redirect('/')
        else:
            return django.http.HttpResponse('Invalid data')
    else:
        task=database.load_tasks(id=id)[0]
        shared_users=' '.join([u.login for u in task.shared_users])
        form=forms.TaskForm(initial={'name':task.name, 
                                     'execute_time':task.execute_time.strftime('%Y-%m-%dT%H:%M')
                                        if task.execute_time is not None else '',
                                     'duration':task.duration,
                                     'tags':' '.join(task.tags),
                                     'priority':task.priority,
                                     'shared_users':shared_users,
                                     'completed':task.completed,
                                     })
        return render(request, 'tasker/task_show.html', {'form':form, 
                                                         'task':task})

def user_show(request, id):
    user=database.load_user(id=id)
    return render(request, 'tasker/user.html', {'user':user})

def add_task(request):
    task=lib.Task()
    database.save_task(task)
    return django.shortcuts.redirect('/task/%d' % task.id)

def add_subtask(request, id):
    task=lib.Task()
    overtask=database.load_tasks(id=id)[0]
    task.overtask=overtask
    database.save_task(task)
    return django.shortcuts.redirect('/task/%d' % task.id)

def repeat(request, id):
    database.init_database(settings.DATABASE_FILE)     
    if request.method=='GET':
        repeat=database.load_tasks(id=id)[0].repeat
        when=None
        if repeat.type==repeat.INTERVAL:
            when=str(repeat.when)
        else:
            when=' '.join([str(w) for w in repeat.when])
        form=forms.RepeatForm(initial={'type':repeat.type,
                                       'when':when})
        return render(request, 'tasker/repeat.html', {'repeat':repeat,
                                                      'form':form})
    else:
        form=forms.RepeatForm(request.POST)
        if form.is_valid():
            task=database.load_tasks(id=id)[0]
            
        else:
            return django.http.HttpResponse('Invalid data')