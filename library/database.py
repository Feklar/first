"""
    This module contains models for tasks and users 
    and functions for load and save
 """

import peewee
import datetime
import pickle

import library.lib as lib
import library.users as users
import sys
import os
#db = peewee.SqliteDatabase(settings.DATABASE_FILE)
db = None
class UserModel(peewee.Model):
    login = peewee.CharField()
    password = peewee.CharField()
    autorized = peewee.BooleanField(default=False)

    def to_user(self):
        user = users.User()
        for key in self.__data__.keys():
            if key in user.__dict__.keys():
                user.__dict__[key] = self.__data__[key]
        return user

    @staticmethod
    def from_user(user):
        if user is None: return None
        model = UserModel()
        for key in model._meta.fields.keys():
            if key in user.__dict__.keys():
                model.__data__[key] = user.__dict__[key]
        return model

    class Meta:
        database = db

def load_user(**kwargs):
    args = [UserModel.__dict__[k].field == v for k, v in kwargs.items()]
    user = UserModel.get(*args)
    user = user.to_user()
    return user

def save_user(user):
    user = UserModel.from_user(user)
    user.save()
    db.commit()

class TaskModel(peewee.Model):
    name = peewee.CharField()
    execute_time = peewee.DateTimeField(null=True)
    duration = peewee.IntegerField(null=True)
    repeat = peewee.BlobField(null=True)
    overtask = peewee.ForeignKeyField('self', null=True, backref='subtasks')
    completed = peewee.BooleanField(default=False)
    creator = peewee.ForeignKeyField(UserModel, null=True)
    priority = peewee.IntegerField(default=0)
    shared_users = peewee.ManyToManyField(UserModel)
    tags = peewee.CharField(max_length=65535, default='')

    def to_task(self):
        task = lib.Task()
        for key in self.__data__.keys():
            if key in task.__dict__.keys():
                task.__dict__[key] = self.__data__[key]
        if self.duration is not None:
            task.duration = datetime.timedelta(seconds=self.duration)
        task.overtask = self.overtask.id if self.overtask is not None else None
        task.subtasks = [s.id for s in self.subtasks]
        task.creator = self.creator.id if self.creator is not None else None
        task.tags = self.tags.split('@')
        task.shared_users = [u.id for u in self.shared_users]
        if self.repeat is not None:
            task.repeat = pickle.loads(self.repeat)
        return task

    @staticmethod
    def from_task(task):
        if task is None: return None
        model = TaskModel()
        model.id = task.id if task.id != 0 else None
        model.name = task.name
        model.execute_time = task.execute_time
        model.duration = task.duration.seconds if task.duration is not None else None
        model.repeat = pickle.dumps(task.repeat)
        model.overtask = TaskModel.from_task(task.overtask)
        model.completed = task.completed
        model.creator = UserModel.from_user(task.creator)
        model.priority = task.priority if task.priority is not None else 0
        model.shared_users.clear()
        for u in task.shared_users:
            model.shared_users.add(UserModel.from_user(u))
        model.tags = '@'.join(task.tags)
        return model

    class Meta:
        database = db

def _load_tasks(loaded_users, loaded_tasks, **kwargs):
    args = [TaskModel.__dict__[k].field == v for k, v in kwargs.items() 
            if k in TaskModel.__dict__.keys() and v is not None]
    if 'tags_any' in kwargs.keys() and kwargs['tags_any'] is not None:
        for tag in kwargs['tags_any']:
            args.append(TaskModel.tags.contains(tag))
    query = TaskModel.select()
    if len(args) > 0:
        query = query.where(*args)
    tasks = []
    for task in query:
        task = task.to_task()
        loaded_tasks[task.id] = task
        tasks.append(task)

    def get_task(id):
        if id not in loaded_tasks.keys():
            loaded_tasks[id] = _load_tasks(loaded_users={}, loaded_tasks=loaded_tasks, id=id)[0]
        return loaded_tasks[id]

    def get_user(id):
        if id not in loaded_users.keys():
            loaded_users[id] = load_user(id=id)
        return loaded_users[id]

    for task in tasks:
        task.shared_users = [get_user(u) for u in task.shared_users]
        if task.creator is not None:
            task.creator = get_user(task.creator)
        if type(task.overtask) is int:
            task.overtask = get_task(task.overtask)
        task.subtasks = [get_task(s) for s in task.subtasks]
    
    tasks.sort(key=(lambda task: task.priority), reverse=True)
    return tasks

def load_tasks(**kwargs):
    return _load_tasks(loaded_users={}, loaded_tasks={}, **kwargs)

def _save_task(task, saved_tasks_ids):
    if task.id not in saved_tasks_ids:
        task_model = TaskModel.from_task(task)
        task_model.save()
        task.id=task_model.id
        saved_tasks_ids.append(task.id)
    if task.overtask is not None:
        _save_task(task.overtask, saved_tasks_ids)
    #for s in task.subtasks:
    #    _save_task(s, saved_tasks_ids)

def save_task(task):
    _save_task(task, [])

def init_database(database_path):
    db=peewee.SqliteDatabase(database_path)
    TaskModel._meta.database=db
    UserModel._meta.database=db
    TaskModel.shared_users.get_through_model()._meta.database=db
    if not db.table_exists('taskmodel'):
        db.create_tables([TaskModel])
    if not db.table_exists('usermodel'):
        db.create_tables([UserModel])
    if not db.table_exists('taskmodel_usermodel_through'):
        db.create_tables([TaskModel.shared_users.get_through_model()])