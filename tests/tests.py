import library.lib as lib
import library.database as db
import unittest
import datetime
import settings

class RepeatTest(unittest.TestCase):
    def test_interval(self):
        db.init_database(settings.DATABASE_FILE)
        repeat = lib.Repeat(interval=datetime.timedelta(hours=3, minutes=55))
        dt = datetime.datetime(2018, 6, 8, 0, 20, 42)
        self.assertEqual(repeat.time_to(dt), datetime.timedelta(hours=3, minutes=55))

    def test_weekdays(self):
        db.init_database(settings.DATABASE_FILE)
        repeat = lib.Repeat(weekdays=[3, 5])
        dt = datetime.datetime(2018, 6, 8, 0, 20, 42)  # Friday (4)
        self.assertEqual(repeat.time_to(dt), datetime.timedelta(days=1))
        dt = datetime.datetime(2018, 6, 9, 0, 20, 42)  # Saturday (5)
        self.assertEqual(repeat.time_to(dt), datetime.timedelta(days=5))
        dt = datetime.datetime(2018, 6, 10, 0, 20, 42)  # Sunday (6)
        self.assertEqual(repeat.time_to(dt), datetime.timedelta(days=4))
        repeat = lib.Repeat(weekdays=[0, 1, 2, 3, 4 ,5 ,6])
        dt = datetime.datetime(2018, 6, 8, 0, 20, 42)  # Friday (4)
        self.assertEqual(repeat.time_to(dt), datetime.timedelta(days=1))
        repeat = lib.Repeat(weekdays=[3]) # Thursday (3)
        dt = datetime.datetime(2018, 6, 14, 11, 58, 28)  # Thursday (3)
        self.assertEqual(repeat.time_to(dt), datetime.timedelta(days=7))

    def test_dates(self):
        db.init_database(settings.DATABASE_FILE)
        repeat = lib.Repeat(dates=[15, 28])
        dt = datetime.datetime(2018, 6, 8, 0, 20, 42)
        self.assertEqual(repeat.time_to(dt), datetime.timedelta(days=7))
        dt = datetime.datetime(2018, 6, 15, 0, 20, 42)
        self.assertEqual(repeat.time_to(dt), datetime.timedelta(days=13))
        dt = datetime.datetime(2018, 6, 28, 0, 20, 42)
        self.assertEqual(repeat.time_to(dt), datetime.timedelta(days=17))


class TaskTest(unittest.TestCase):
    def test_load_one(self):
        db.init_database(settings.DATABASE_FILE)
        tasks=db.load_tasks(id=5)
        self.assertEqual(len(tasks), 1)
        t=tasks[0]
        self.assertEqual(t.name, 'test task')
        self.assertIsNone(t.overtask)
        self.assertIsNone(t.creator)
        self.assertEqual(t.execute_time, datetime.datetime(2018, 8, 27, 20, 33))
        self.assertEqual(t.duration, datetime.timedelta(hours=3, minutes=14, seconds=15))
        self.assertEqual(t.tags, ['tag1', 'tag2'])
        self.assertEqual(t.priority, 4)
        self.assertEqual(t.repeat.type, lib.Repeat.DATE)
        self.assertEqual(t.repeat.when, [3, 13, 23])
        self.assertEqual(len(t.shared_users), 1)
        self.assertEqual(t.shared_users[0].id, 2)

    def test_load_multiple(self):
        db.init_database(settings.DATABASE_FILE)
        tasks = db.load_tasks(id=2)
        self.assertEqual(len(tasks), 1)
        t11 = tasks[0]
        self.assertEqual(t11.name, 't11')
        t1 = t11.overtask
        self.assertEqual(t1.name, 't1')
        t11s = t11.subtasks
        self.assertEqual(len(t11s), 1)
        t111=t11s[0]
        self.assertEqual(t111.name, 't111')
        t1s = t1.subtasks
        self.assertEqual(len(t1s), 2)
        t1s.sort(key=lambda t:t1.name)
        t12 = t1s[1]
        self.assertEqual(t12.name, 't12')
        self.assertIs(t1.overtask, None)
        self.assertIs(t11.overtask, t1)
        self.assertIs(t12.overtask, t1)
        self.assertIs(t111.overtask, t11)

        self.assertIn(t11, t1.subtasks)
        self.assertIn(t12, t1.subtasks)
        self.assertIn(t111, t11.subtasks)

    def test_repeat(self):
        db.init_database(settings.DATABASE_FILE)
        tasks=db.load_tasks(id=5)
        self.assertEqual(len(tasks), 1)
        t=tasks[0]
        t1=t.get_repeated()
        self.assertEqual(t1.name, t.name)
        self.assertEqual(t1.creator, t.creator)
        self.assertEqual(t1.overtask, t.overtask)
        self.assertEqual(t1.execute_time, datetime.datetime(2018, 9, 3, 20, 33))
        self.assertEqual(t1.duration, t.duration)
        self.assertFalse(t.completed)
        self.assertEqual(t1.tags, t.tags)
        self.assertEqual(t1.priority, t.priority)
        t2=t1.get_repeated()
        self.assertEqual(t2.execute_time, datetime.datetime(2018, 9, 13, 20, 33))
        t3=t2.get_repeated()
        self.assertEqual(t3.execute_time, datetime.datetime(2018, 9, 23, 20, 33))
        t4=t3.get_repeated()
        self.assertEqual(t4.execute_time, datetime.datetime(2018, 10, 3, 20, 33))

if __name__ == '__main__':
    unittest.main(argv=['first-arg-is-ignored'], exit=False)