import logging
import os

DATABASE_FILE = os.path.join(os.path.dirname(__file__), 'Test.db')
LOG_FILE = os.path.join(os.path.dirname(__file__), 'test.log')
LOG_LEVEL = logging.NOTSET
