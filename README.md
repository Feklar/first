Tasker is application for work with tasks. You can create some task hierarchy and monitor their execution. Tasker supports multi-user work: tasks can have creators and users with shared access.
  
# Installing
```bash
$ git clone https://bitbucket.org/Feklar/first
$ cd first
$ python3 setup.py install
$ tasker init_database
```

# Examples of work
## Create a new task
```bash
tasker task add -n task_name -w "27.08.2018 14:00:13"
```

## Edit task
```bash
tasker task -t task_name edit -d 3:14:15
```

## Add tags
```bash
tasker task -t task_name edit -g tag1 tag2
```

## Show tasks with tags
```bash
tasker task show -g tag1
```

## Create user
```bash
tasker user add -l user_login -p 1234
```

## Authorize
```bash
tasker user autorize -l user_login -p 1234
```

## Set creator of task:
```bash
tasker task create -n task_name -c user_login
```

## Share access:
```bash
tasker task -t task_name share -u user_1_name user_2_name
